'use strict'

var models = require('../models');
var persona = models.persona;
var rol = models.rol;
var cuenta = models.cuenta;
let jwt = require('jsonwebtoken');

class CuentaControl {

    async inicio_sesion(req, res) {
        if (req.body.hasOwnProperty('correo') &&
            req.body.hasOwnProperty('clave')) {
            let cuentaA = await cuenta.findOne({
                where: { correo: req.body.correo },
                include: [
                    { model: models.persona, as: "persona", attributes: ["apellidos", "nombres"] }
                ]
            });

            if (cuentaA === null) {
                res.status(400);
                res.json({ msg: "Error", tag: "Cuenta no existe", code: 400 });
            } else {
                if (cuentaA.estado === true) {
                    if (cuentaA.clave === req.body.clave) {
                        const token_data = {
                            //se pueden enviar los permisos o rol.....
                            external: cuentaA.external_id,
                            check: true,
                        };

                        //el tiempo depende de la aplicacion
                        require('dotenv').config();
                        const key = process.env.KEY_PRI;
                        const token = jwt.sign(token_data, key, {
                            expiresIn: '2h'
                        });

                        var info = {
                            token: token,
                            user: cuentaA.persona.apellidos + ' ' + cuentaA.persona.nombres
                        }
                        res.status(200);
                        res.json({ msg: "OK", tag: "Listo", datos: info, code: 200 });
                    } else {
                        res.status(400);
                        res.json({ msg: "Error", tag: "Correo o clave incorrectos", code: 400 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Error", tag: "Cuenta desactivada", code: 400 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }
}
module.exports = CuentaControl;
