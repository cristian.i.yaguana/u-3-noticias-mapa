'use strict'

var models = require('../models');
var persona = models.persona;
var noticia = models.noticia;
var rol = models.rol;
var cuenta = models.cuenta;

class PersonaControl {

    async listar(req, res) {
        var lista = await persona.findAll({
            attributes: ["nombres", "apellidos", "celular", "direccion", "fecha_nac", ["external_id", "id"]],
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ["correo"] },
                { model: models.rol, as: "rol", attributes: ["nombre"] }
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external },
            attributes: ["nombres", "apellidos", "celular", "direccion", "fecha_nac", ["external_id", "id"]],
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ["correo"] },
                { model: models.rol, as: "rol", attributes: ["nombre"] }
            ]
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {

        if (req.body.hasOwnProperty('nombres') &&
            req.body.hasOwnProperty('apellidos') &&
            req.body.hasOwnProperty('direccion') &&
            req.body.hasOwnProperty('celular') &&
            req.body.hasOwnProperty('fecha') &&
            req.body.hasOwnProperty('rol')) {

            var uuid = require('uuid');
            var rol_aux = await rol.findOne({ where: { external_id: req.body.rol } });

            if (rol_aux != undefined) {

                var data = {
                    nombres: req.body.nombres,
                    apellidos: req.body.apellidos,
                    direccion: req.body.direccion,
                    celular: req.body.celular,
                    fecha_nac: req.body.fecha,
                    id_rol: rol_aux.id,
                    external_id: uuid.v4(),
                    cuenta: {
                        correo: req.body.correo,
                        clave: req.body.clave
                    }
                }
                let transaction = await models.sequelize.transaction();

                try {
                    //se guarda primero la fuerte y luego la debil
                    var result = await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                    await transaction.commit();
                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "Error", tag: "no se puede crear", code: 401 });
                    } else {
                        rol_aux.external_id = uuid.v4();
                        await rol_aux.save();
                        res.status(200);
                        res.json({ msg: "OK", code: 200 });
                    }

                } catch (error) {
                    if (transaction) await transaction.rollback();
                    res.status(203);
                    res.json({ msg: "Error", code: 203, error_msg: error.errors[0].message });
                }


            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "El dato a buscar no existe", code: 400 });
            }

        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }

    async modificar(req, res) {
        var person = await persona.findOne({ where: { external_id: req.body.external } })

        if (person === null) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {

            if (req.body.hasOwnProperty('nombres') &&
                req.body.hasOwnProperty('apellidos') &&
                req.body.hasOwnProperty('direccion') &&
                req.body.hasOwnProperty('celular') &&
                req.body.hasOwnProperty('fecha')) {

                var uuid = require('uuid')

                person.nombres = req.body.nombres;
                person.apellidos = req.body.apellidos;
                person.direccion = req.body.direccion;
                person.celular = req.body.celular;
                person.fecha_nac = req.body.fecha;
                person.external_id = uuid.v4();

                var result = await person.save();

                if (result === null) {
                    res.status(400);
                    res.json({ msg: "Error", tag: "No se han modificado los datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Datos modificados correctamente", code: 200 });
                }


            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "faltan datos", code: 400 });
            }
        }
    }
    async banear(req, res) {
        const [apellidos, nombres] = req.params.external.split(' ');

        var personaAux = await persona.findOne({
            where: { apellidos, nombres },
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['external_id', "estado"] },
            ],
        });

        if (personaAux === null || personaAux === undefined) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a banear no existe", code: 400 });
        } else {
            var uuid = require('uuid');
            var cuentaAux = await cuenta.findOne({ where: { external_id: personaAux.cuenta.external_id } });
            cuentaAux.estado = false;
            cuentaAux.external_id = uuid.v4();
            personaAux.external_id = uuid.v4();
            var cuentabann = await cuentaAux.save();
            var personbann = await personaAux.save();

            if (cuentabann === null && personbann === null) {
                res.status(400);
                res.json({ msg: "Error", tag: "No se pudo banear al usuario", code: 400 });
            } else {
                res.status(200);
                res.json({ msg: "Success", tag: "Usuario baneado correctamente", code: 200 });
            }

        }
    }


    async guardar_Usuario(req, res) {

        if (req.body.hasOwnProperty('nombres') &&
            req.body.hasOwnProperty('apellidos') &&
            req.body.hasOwnProperty('correo') &&
            req.body.hasOwnProperty('clave') ) {

            var uuid = require('uuid');

            var rolAux = await rol.findOne({ where: { nombre: "usuario" } });

            var data = {
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                id_rol: rolAux.id,
                external_id: uuid.v4(),
                cuenta: {
                    correo: req.body.correo,
                    clave: req.body.clave
                }
            }

            let transaction = await models.sequelize.transaction();

            try {
                //se guarda primero la fuerte y luego la debil
                var result = await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                await transaction.commit();
                if (result === null) {
                    res.status(401);
                    res.json({ msg: "Error", tag: "no se puede crear", code: 401 });
                } else {
                    rolAux.external_id = uuid.v4();
                    await rolAux.save();
                    res.status(200);
                    res.json({ msg: "OK", tag: "Usuario creado correctamente",code: 200 });
                }

            } catch (error) {
                if (transaction) await transaction.rollback();
                res.status(203);
                res.json({ msg: "Error", code: 203, error_msg: error.errors[0].message });
            }

        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }
}


module.exports = PersonaControl;

// modificar datos persona
