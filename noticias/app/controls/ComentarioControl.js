'use strict'

var models = require('../models');
var persona = models.persona;
var noticia = models.noticia;
var comentario = models.comentario;





class ComentarioControl {
    async listar(req, res) {
        var lista = await comentario.findAll({
            attributes: ["texto", "estado", "longitud", "latitud", "usuario", "external_id"],
            include: [
                { model: models.noticia, as: "noticium",  where: { external_id: req.params.external },attributes: ["titulo", "tipo_noticia"] },
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }
    async listarcomentarioscompletos(req, res) {
        var lista = await comentario.findAll({
            attributes: ["texto", "estado", "longitud", "latitud", "usuario", "external_id"],
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async guardar(req, res) {
        if (req.body.hasOwnProperty('texto') &&
            req.body.hasOwnProperty('longitud') &&
            req.body.hasOwnProperty('latitud') &&
            req.body.hasOwnProperty('usuario') &&
            req.body.hasOwnProperty('noticia')) {
            var uuid = require('uuid');
            var notiAux = await noticia.findOne({ where: { external_id: req.body.noticia } });

            if (notiAux != undefined) {

                var data = {
                    texto: req.body.texto,
                    longitud: req.body.longitud,
                    latitud: req.body.latitud,
                    usuario: req.body.usuario,
                    id_noticia: notiAux.id,
                    external_id: uuid.v4(),
                }
                let transaction = await models.sequelize.transaction();

                try {
                    var result = await comentario.create(data, { transaction });
                    await transaction.commit();
                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "Error", tag: "no se puede crear", code: 401 });
                    } else {
                        notiAux.external_id = uuid.v4();
                        await notiAux.save();
                        res.status(200);
                        res.json({ msg: "OK", tag: "comentario guardado con exito", code: 200 });
                    }

                } catch (error) {
                    if (transaction) await transaction.rollback();
                    res.status(203);
                    console.log(error)
                    res.json({ msg: "Error", code: 203, tag: error.errors });
                }


            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "El dato a buscar no existe", code: 400 });
            }

        } else {
            res.status(400);
            res.json({ msg: "Error", tag: "faltan datos", code: 400 });
        }

    }


    async modificar(req, res) {
        var com = await comentario.findOne({ where: { external_id: req.body.external } })

        if (com === null) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {

            if (req.body.hasOwnProperty('texto') &&
                req.body.hasOwnProperty('longitud') &&
                req.body.hasOwnProperty('latitud')) {

                var uuid = require('uuid')

                com.texto = req.body.texto;
                com.longitud = req.body.longitud;
                com.latitud = req.body.latitud;
                com.external_id = uuid.v4();

                var result = await com.save();

                if (result === null) {
                    res.status(400);
                    res.json({ msg: "Error", tag: "No se han modificado los datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Datos modificados correctamente", code: 200 });
                }

            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "faltan datos", code: 400 });
            }
        }
    }


}
module.exports = ComentarioControl;
