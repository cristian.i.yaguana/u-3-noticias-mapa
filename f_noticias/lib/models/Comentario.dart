import 'dart:developer';

class Comentario {
  String texto = '';
  String longitud = '';
  String latitud = '';
  String usuario = '';
  bool estado = false;
  String external = '';
  

  Comentario();

  Comentario.fromMap(Map<dynamic, dynamic> mapa) {
    texto = mapa['texto'];
    longitud = mapa['longitud'];
    latitud = mapa['latitud'];
    usuario = mapa['usuario'];
    external = mapa['external_id'];
    estado = (mapa['estado'].toString() == 'true') ? true : false;
  }
}
