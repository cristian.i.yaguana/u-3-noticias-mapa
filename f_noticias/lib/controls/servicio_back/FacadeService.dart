import 'dart:convert';
import 'dart:developer';
import 'package:f_noticias/controls/Conexion.dart';
import 'package:f_noticias/controls/servicio_back/RespuestaGenerica.dart';
import 'package:f_noticias/controls/servicio_back/modelo/ComentarioSW.dart';
import 'package:f_noticias/controls/servicio_back/modelo/InicioSesionSW.dart';
import 'package:f_noticias/controls/utiles/Utiles.dart';
import 'package:f_noticias/models/Comentario.dart';
import 'package:http/http.dart' as http;
import 'package:f_noticias/controls/servicio_back/modelo/ListaNoticiaSW.dart';
// aqui se hacen las conexion con el backend
class FacadeService {
  Conexion c = Conexion();

  Future<InicioSesionSW> inicioSesion(Map<String, String> mapa) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}login';
    final uri = Uri.parse(url);

    InicioSesionSW isw = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw.code = 404;
          isw.msg = 'Error';
          isw.tag = 'Recurso no encontrado';
          isw.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          isw.code = mapa['code'];
          isw.msg = mapa['msg'];
          isw.tag = mapa['tag'];
          isw.datos = mapa['datos'];
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        isw.code = mapa['code'];
        isw.msg = mapa['msg'];
        isw.tag = mapa['tag'];
        isw.datos = mapa['datos'];
      }
    } catch (e) {
      isw.code = 500;
      isw.msg = 'Error';
      isw.tag = 'Error inesperado';
      isw.datos = {};
    }
    return isw;
  }

  Future<InicioSesionSW> registrarUsuario(Map<String, String> mapa) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}admin/usuarios/guardar';
    final uri = Uri.parse(url);

    InicioSesionSW isw = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw.code = 404;
          isw.msg = 'Error';
          isw.tag = 'Recurso no encontrado';
          isw.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          isw.code = mapa['code'];
          isw.msg = mapa['msg'];
          isw.tag = mapa['tag'];
          isw.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        isw.code = mapa['code'];
        isw.msg = mapa['msg'];
        isw.tag = mapa['tag'];
        isw.datos = {};
      }
    } catch (e) {
      isw.code = 500;
      isw.msg = 'Error';
      isw.tag = 'Error inesperado';
      isw.datos = {};
    }
    return isw;
  }
  Future<ListaComentarioWS> listarcomentario(String external) async {
  Utiles util = Utiles();
  var token = await util.getValue("token");

  Map<String, String> header = {
    'Content-Type': 'application/json',
    'news-token': token ?? '',
  };

  final String url = '${c.URL}comentarios/$external';
  final uri = Uri.parse(url);

  ListaComentarioWS isw = ListaComentarioWS();

  try {
    final response = await http.get(uri, headers: header);

    if (response.statusCode != 200) {
      if (response.statusCode == 404) {
        isw = ListaComentarioWS.fromMap([], 'Error recurso no encontrado ', 404);
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);
        isw = ListaComentarioWS.fromMap(
            [], mapa["msg"], int.parse(mapa["code"].toString()));
      }
    } else {
      Map<String, dynamic> mapa = jsonDecode(response.body);
      List datos = jsonDecode(jsonEncode(mapa["datos"]));
      isw = ListaComentarioWS.fromMap(
          datos, mapa["msg"], int.parse(mapa["code"].toString()));
    }
  } catch (e) {
    isw = ListaComentarioWS.fromMap([], 'Error $e', 500);
  }
  log(isw.toString());
  return isw;
}


 Future<ListaNoticiaSW> listarAllNoticias() async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}listarnoticias';
    final uri = Uri.parse(url);

    ListaNoticiaSW isw = ListaNoticiaSW();

    try {
      final response = await (await (http.get(uri, headers: header)));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw = ListaNoticiaSW.fromMap([], 'Error recurso no encontrado ', 404);
        } else {
          Map<String, dynamic> mapa = jsonDecode(response.body);
          isw = ListaNoticiaSW.fromMap(
              [], mapa["msg"], int.parse(mapa["code"].toString()));
        }
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);
        List datos = jsonDecode(jsonEncode(mapa["datos"]));
        isw = ListaNoticiaSW.fromMap(
            datos, mapa["msg"], int.parse(mapa["code"].toString()));
      }
    } catch (e) {
      isw = ListaNoticiaSW.fromMap([], 'Error $e', 500);
    }
    log(isw.toString());
    return isw;
  }

  Future<InicioSesionSW> registrarComentario(Map<String, String> mapa) async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}/admin/comentarios/registrar';
    final uri = Uri.parse(url);

    InicioSesionSW isw = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw.code = 404;
          isw.msg = 'Error';
          isw.tag = 'Recurso no encontrado';
          isw.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          isw.code = mapa['code'];
          isw.msg = mapa['msg'];
          isw.tag = mapa['tag'];
          isw.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        isw.code = mapa['code'];
        isw.msg = mapa['msg'];
        isw.tag = mapa['tag'];
        isw.datos = {};
      }
    } catch (e) {
      isw.code = 500;
      isw.msg = 'Error';
      isw.tag = 'Error inesperado';
      isw.datos = {};
    }
    return isw;
  }


  




}

