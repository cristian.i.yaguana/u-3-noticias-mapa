
import 'dart:developer';
import 'package:f_noticias/controls/servicio_back/RespuestaGenerica.dart';
import 'package:f_noticias/models/Noticias.dart';

class ListaNoticiaSW extends RespuestaGenerica {
  late List<Noticia> data = [];
  ListaNoticiaSW();
  ListaNoticiaSW.fromMap(List datos, String msg, int code) {
    datos.forEach((item) {
      Map<String, dynamic> mapa = item;
      Noticia aux = Noticia.fromMap(mapa);
      data.add(aux);
    });

    this.msg = msg;
    this.code = code;
  }


}
