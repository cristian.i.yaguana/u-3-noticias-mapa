import 'dart:developer';

import 'package:f_noticias/controls/servicio_back/RespuestaGenerica.dart';
import 'package:f_noticias/models/Comentario.dart';

class ListaComentarioWS extends RespuestaGenerica {
  late List<Comentario> data = [];

  ListaComentarioWS();

  ListaComentarioWS.fromMap(List datos, String msg, int code) {
    datos.forEach((item) {
      Map<String, dynamic> mapa = item;
      Comentario aux = Comentario.fromMap(mapa);
      data.add(aux);
    });

    this.msg = msg;
    this.code = code;
  }
}
