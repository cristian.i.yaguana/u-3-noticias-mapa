import 'dart:convert';
import 'package:f_noticias/controls/servicio_back/RespuestaGenerica.dart';
import 'package:http/http.dart' as http;
import 'package:f_noticias/controls/utiles/Utiles.dart';

class Conexion {
  final String URL = 'http://10.20.143.73:3000/api/';
  static final String URL_MEDIA = 'http://10.20.143.73:3000/multimedia/';
  static bool NO_TOKEN = false;
  //news-token

  Future<RespuestaGenerica> solicitudGet(String recurso, bool token) async {
    Map<String, String> _header = {'Content-Type': 'application/json'};
    if (token == true) {
      Utiles util = Utiles();
      String? tokenA = await util.getValue('token');
      _header = {
        'Content-Type': 'application/json',
        'news-token': tokenA.toString()
      };
    }

    final String _url = URL + recurso;
    final uri = Uri.parse(_url);

    try {
      final response = await http.get(uri, headers: _header);
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return _response(404, "Recurso no encontrado", []);
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return _response(mapa['code'], mapa['msg'], mapa['datos']);
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return _response(mapa['code'], mapa['msg'], mapa['datos']);
      }
      // return RespuestaGenerica();
    } catch (e) {
      return _response(500, "Error Inesperado", []);
    }
  }

  Future<RespuestaGenerica> solicitudPost(
      String recurso, bool token, Map<dynamic, dynamic> data) async {
    Map<String, String> _header = {'Content-Type': 'application/json'};
    if (token == true) {
      _header = {'Content-Type': 'application/json', 'news-token': 'afea'};
    }

    final String _url = URL + recurso;
    final uri = Uri.parse(_url);

    try {
      final response = await http.post(uri, headers: _header);
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return _response(404, "Recurso no encontrado", []);
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return _response(mapa['code'], mapa['msg'], mapa['datos']);
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return _response(mapa['code'], mapa['msg'], mapa['datos']);
      }
      // return RespuestaGenerica();
    } catch (e) {
      return _response(500, "Error Inesperado", []);
    }
  }

  RespuestaGenerica _response(int code, String msg, dynamic data) {
    var respuesta = RespuestaGenerica();
    respuesta.code = code;
    respuesta.msg = msg;
    respuesta.datos = data;
    return respuesta;
  }
}
