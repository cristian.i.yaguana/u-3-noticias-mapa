import 'package:f_noticias/controls/servicio_back/FacadeService.dart';
import 'package:f_noticias/models/Comentario.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class MapaView extends StatefulWidget {
  const MapaView({Key? key}) : super(key: key);

  @override
  _MapaViewState createState() => _MapaViewState();
}

class _MapaViewState extends State<MapaView> {
  static const MAPBOX_ACCESS_TOKEN =
      'pk.eyJ1IjoiY3Jpc3N5YWdnMjAwMSIsImEiOiJjbHN0eXN4azUxcTFxMmtud20wZXRhYjF1In0.m63alyp-56HVsTeOtp5fKA';

  MapOptions _createMapOptions() {
    return MapOptions(
      center: LatLng(-3.9834385, -79.2159910),
      minZoom: 2,
      maxZoom: 20,
      zoom: 12,
    );
  }

  Future<List<Comentario>> _listar() async {
    // Aquí deberías implementar la lógica para obtener los comentarios
    // Puedes hacer llamadas a servicios o cargarlos de otro lugar
    return Future.value([]); // Por ahora, devolvemos una lista vacía
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Mapa'),
        backgroundColor: Colors.blueAccent,
      ),
      body: FlutterMap(
        options: _createMapOptions(),
        nonRotatedChildren: [
          TileLayer(
            urlTemplate:
                'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
            additionalOptions: const {
              'accessToken': MAPBOX_ACCESS_TOKEN,
              'id': 'mapbox/streets-v12'
            },
          ),
          FutureBuilder(
            future: _listar(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return MarkerLayer(
                  markers: _crearMarcadores(snapshot.data),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ],
      ),
    );
  }
}

List<Color> colores = [
  Colors.blueAccent,
  Colors.red,
  Colors.green,
  Color.fromARGB(255, 107, 41, 10),
  Colors.white,
  Color.fromARGB(255, 37, 161, 132),
  Color.fromARGB(255, 136, 255, 0),
  const Color.fromARGB(255, 0, 0, 0),
  Color.fromARGB(255, 18, 143, 152),
  Color.fromARGB(183, 31, 17, 89),
  const Color.fromARGB(255, 244, 155, 54)
];
List<Marker> _crearMarcadores(List<Comentario> comentarios) {
  // Utilizamos valores quemados para la latitud y longitud del marcador
  double latitud = -4.0287364000;
  double longitud = -79.2023343000;

  return comentarios.asMap().entries.map((entry) {
    int index = entry.key;
    Comentario coment = entry.value;
    return Marker(
      point: LatLng(-4.0287364000, -79.2023343000), // Usamos las coordenadas proporcionadas
      builder: (context) {
        return Container(
          child: ListTile(
            leading: Icon(
              Icons.location_on,
              color: colores[index % colores.length],
              size: 40,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    icon: Icon(
                      Icons.person_pin,
                      color: Colors.green,
                      size: 50.0,
                    ),
                    content: Text("COMENTARIO REGISTRADO"),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Cerrar"),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        );
      },
    );
  }).toList();
}
