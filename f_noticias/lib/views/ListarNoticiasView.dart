import 'package:flutter/material.dart';
import 'dart:developer';
import 'package:f_noticias/controls/Conexion.dart';
import 'package:f_noticias/controls/servicio_back/FacadeService.dart';
import 'package:f_noticias/controls/utiles/Utiles.dart';
import 'package:f_noticias/models/Noticias.dart';

class ListarNoticiasView extends StatefulWidget {
  const ListarNoticiasView({Key? key}) : super(key: key);

  @override
  _ListarNoticiasViewState createState() => _ListarNoticiasViewState();
}

class _ListarNoticiasViewState extends State<ListarNoticiasView> {
  late Future<List<Noticia>> _noticiasFuture;

  @override
  void initState() {
    super.initState();
    _noticiasFuture = _listar();
  }

  Future<List<Noticia>> _listar() async {
    FacadeService servicio = FacadeService();

    try {
      var value = await servicio.listarAllNoticias();
      print("La data es ===============================================");
      debugPrint("El código de la respuesta es: ${value.code}");

      if (value.code == 200) {
        return value.data;
      } else {
        Utiles util = Utiles();
        util.removeAllItem();
        Navigator.pushNamed(context, '/home');
        return [];
      }
    } catch (e) {
      log("Error al cargar noticias: $e");
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista de Noticias"),
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: FutureBuilder<List<Noticia>>(
          future: _noticiasFuture,
          builder: (BuildContext context, AsyncSnapshot<List<Noticia>> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    child: NoticiasCard(snapshot.data![index]),
                  );
                },
              );
            } else {
              return Center(child: Text('No hay datos disponibles'));
            }
          },
        ),
      ),
      bottomNavigationBar: Row(
        children: <Widget>[
          const Text("LOGIN: "),
          TextButton(
            onPressed: _cerrarSesion,
            child: const Text(
              "CERRAR SESION",
              style: TextStyle(fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }

  void _cerrarSesion() {
    Utiles util = Utiles();
    util.removeAllItem();
    Navigator.pushNamed(context, "/home");
  }
}

class NoticiasCard extends StatelessWidget {
  final Noticia noticia;

  // Constructor con parámetro obligatorio
  NoticiasCard(this.noticia);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Image.network('${Conexion.URL_MEDIA}${noticia.archivo}'),
          ListTile(
            title: Text(
              'NOTICIA: ${noticia.titulo}',
              overflow: TextOverflow.ellipsis,
            ),
            subtitle: Text(
              'CATEGORIA: ${noticia.tipo_noticia}\nContenido: ${noticia.cuerpo}',
              overflow: TextOverflow.ellipsis,
            ),
          ),
          // Agregamos el botón para comentar
          TextButton(
            onPressed: () {
              print("Valor de noticia.id: ${noticia.id}");
              Navigator.pushNamed(
                //aqui fijarse para el direccionamiento
                context,
                "/registrarcoment",
                arguments: {'external': noticia.id}, 
              );
            },
            child: Text('Comentar'),
          ),
        ],
      ),
    );
  }
}
