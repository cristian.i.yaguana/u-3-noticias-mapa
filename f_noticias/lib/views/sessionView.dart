import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:f_noticias/controls/servicio_back/FacadeService.dart';
import 'package:f_noticias/controls/utiles/Utiles.dart';
import 'package:validators/validators.dart';
//import 'package:noticias/controls/Conexion.dart';

class SessionView extends StatefulWidget {
  const SessionView({Key? key}) : super(key: key);

  @override
  _SessionViewState createState() => _SessionViewState();
}

class _SessionViewState extends State<SessionView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController correoControl = TextEditingController();
  final TextEditingController claveControl = TextEditingController();

  void _iniciar() {
    setState(() {
      FacadeService servicio = FacadeService();

      if (_formKey.currentState!.validate()) {
        Map<String, String> mapa = {
          "correo": correoControl.text,
          "clave": claveControl.text
        };

        servicio.inicioSesion(mapa).then((value) async {
          if (value.code == 200) {
            Utiles util = Utiles();
            util.setValue('token', value.datos['token']);
            util.setValue('user', value.datos['user']);

            final SnackBar msg =
                  SnackBar(content: Text('BIENVENIDO ${value.datos['user']}'));
              ScaffoldMessenger.of(context).showSnackBar(msg);
              Navigator.pushNamed(context, "/listarnoticias");

          } else {
            final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
            ScaffoldMessenger.of(context).showSnackBar(msg);
          }
        });
      } else {
        log("Error");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "Noticias",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "La mejor app de Noticias",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "Inicio de Sesion",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoControl,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar su correo";
                  }
                  if (!isEmail(value)) {
                    return "Debe ingresar un correo valido";
                  }
                },
                decoration: const InputDecoration(
                    labelText: 'Correo',
                    suffixIcon: Icon(Icons.alternate_email)),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: claveControl,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar su clave";
                  }
                },
                decoration: const InputDecoration(
                    labelText: 'Clave', suffixIcon: Icon(Icons.key)),
                obscureText: true,
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text("Inicio"),
                onPressed: () => (_iniciar()),
              ),
            ),
            Row(
              children: <Widget>[
                const Text("No tienes una cuenta"),
                TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, "/register");
                    },
                    child: const Text(
                      "Registrate",
                      style: TextStyle(fontSize: 20),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

/*
TEMA 1: MAPAS
implementar geolocalizacion, en android activar en android manifest
activar permisos para internet

solo dar permisos, no se debe instalar nada
dar permisos de usuario
guardar comentario
crear mapa con left
al registrar comentario receptar latitud y longitud

//metodos

low '' no es preciso
medium - mas decente 

utilizar cualquier libreria
//geoloquer
//location --- bajar requerimientos al flutter
//geo-location --- crear canales para retornar latitud y longitud
//
//
//conectarse a la red   son dos lineas de codigo para dar permisos

Guardar comentarios mostrar puntos en el mapa de los comentarios

*/
