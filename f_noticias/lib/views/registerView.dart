import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:f_noticias/controls/servicio_back/FacadeService.dart';
import 'package:f_noticias/controls/utiles/Utiles.dart';
import 'package:validators/validators.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController nombreC = TextEditingController();
  final TextEditingController apellidoC = TextEditingController();
  final TextEditingController correoC = TextEditingController();
  final TextEditingController claveC = TextEditingController();

  void _registrar() {
    setState(() {
      FacadeService servicio = FacadeService();

      if (_formKey.currentState!.validate()) {
        Map<String, String> mapa = {
          "nombres": nombreC.text,
          "apellidos": apellidoC.text,
          "correo": correoC.text,
          "clave": claveC.text
        };

        servicio.registrarUsuario(mapa).then((value) async {
          if (value.code == 200) {
            final SnackBar msg =
                SnackBar(content: Text('Success: ${value.tag}'));
            ScaffoldMessenger.of(context).showSnackBar(msg);
            Navigator.pushNamed(context, "/home");
          } else {
            final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
            ScaffoldMessenger.of(context).showSnackBar(msg);
          }
        });
      } else {
        log("Error");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "Registro Usuarios",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "La mejor app de Noticias",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                  controller: nombreC,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Debe ingresar sus nombres";
                    }
                  },
                  decoration: const InputDecoration(labelText: 'Nombres')),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: apellidoC,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar sus apellidos";
                  }
                },
                decoration: const InputDecoration(labelText: 'Apellidos'),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoC,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar su correo";
                  }
                  if (!isEmail(value)) {
                    return "Debe ingresar un correo valido";
                  }
                },
                decoration: const InputDecoration(
                    labelText: 'Correo',
                    suffixIcon: Icon(Icons.alternate_email)),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: claveC,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar su clave";
                  }
                },
                decoration: const InputDecoration(
                    labelText: 'Clave', suffixIcon: Icon(Icons.key)),
                obscureText: true,
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text("Registrar"),
                onPressed: () => (_registrar()),
              ),
            ),
            Row(
              children: <Widget>[
                const Text("Ya tienes una cuenta"),
                TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, "/home");
                    },
                    child: const Text(
                      "Inicio de Sesion",
                      style: TextStyle(fontSize: 20),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
