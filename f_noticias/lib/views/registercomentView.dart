import 'dart:developer';
import 'package:f_noticias/controls/Conexion.dart';
import 'package:f_noticias/models/Comentario.dart';
import 'package:flutter/material.dart';
import 'package:f_noticias/controls/utiles/Utiles.dart';
import 'package:f_noticias/controls/servicio_back/FacadeService.dart';
import 'package:geolocator/geolocator.dart';



class ComentarioView extends StatefulWidget {
  const ComentarioView({Key? key}) : super(key: key);

  @override
  _ComentarioViewState createState() => _ComentarioViewState();
}

Future<List<Comentario>> _listar(BuildContext context, String externalId) async {
  FacadeService servicio = FacadeService();

  try {
    var value = await servicio.listarcomentario(externalId);
    print("La data es ===============================================");
    debugPrint("El código de la respuesta es: ${value.code}");

    if (value.code == 200) {
      return value.data;
    } else {
      Utiles util = Utiles();
      util.removeAllItem();
      Navigator.pushNamed(context, '/home');
      return [];
    }
  } catch (e) {
    log("Error al cargar comentarios: $e");
    return [];
  }
}

class _ComentarioViewState extends State<ComentarioView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController textoC = TextEditingController();
  double latitud = 0.0;
  double longitud = 0.0;

  Future<Position> _Locacion() async {
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        const SnackBar msg = SnackBar(
            content: Text('Se necesitan permisos de ubicacion para comentar'));
        ScaffoldMessenger.of(context).showSnackBar(msg);
        return Future.error('error');
      }
    }
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  void getCurrentLocation(String externalId) async {
    Position posicion;

    try {
      posicion = await _Locacion();
      print("esta es la posicion:");
      print(posicion);

      latitud = posicion.latitude;
      longitud = posicion.longitude;

      _registrar(externalId);
    } catch (e) {
      print('Error al obtener la posición: $e');
    }
  }

  Future<void> _registrar(String externalId) async {
    FacadeService servicio = FacadeService();
    Utiles util = Utiles();
    var user = await util.getValue("user");

    if (_formKey.currentState!.validate()) {
      Map<String, String> mapa = {
        "texto": textoC.text,
        "longitud": longitud.toString(),
        "latitud": latitud.toString(),
        "usuario": user ?? '',
        "noticia": externalId
      };

      servicio.registrarComentario(mapa).then((value) async {
        if (value.code == 200) {
          final SnackBar msg = SnackBar(content: Text('Success: ${value.tag}'));
          ScaffoldMessenger.of(context).showSnackBar(msg);
          Navigator.pushNamed(context, "/listarnoticias");
        } else {
          final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
          ScaffoldMessenger.of(context).showSnackBar(msg);
        }
      });
    } else {
      log("Error");
    }
  }

 @override
  Widget build(BuildContext context) {
    Map<String, dynamic>? parametro =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;

    String? externalId = parametro?['external'].toString();
    return Form(
      key: _formKey,
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: textoC,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Debe ingresar un comentario";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Ingrese el Comentario:',
                  ),
                ),
              ),
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text("Comentar"),
                  onPressed: () async {
                    getCurrentLocation(externalId!);
                  },
                ),
              ),
              // Mostrar ComentarioCards
              Expanded(
                child: FutureBuilder<List<Comentario>>(
                  future: _listar(context, externalId!),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(child: CircularProgressIndicator());
                    } else if (snapshot.hasError) {
                      return Center(child: Text('Error: ${snapshot.error}'));
                    } else {
                      List<Comentario> comentarios = snapshot.data ?? [];
                      return ListView.builder(
                        itemCount: comentarios.length,
                        itemBuilder: (context, index) {
                          return ComentarioCard(comentarios[index]);
                        },
                      );
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ComentarioCard extends StatelessWidget {
  final Comentario comentario;

  // Constructor con parámetro obligatorio
  ComentarioCard(this.comentario);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          ListTile(
            title: Text(
              'Usuario: ${comentario.usuario}',
              overflow: TextOverflow.ellipsis,
            ),
            subtitle: Text(
              '${comentario.texto}',
              overflow: TextOverflow.ellipsis,
            ),
          ),
          // No hay imagen
          TextButton(
            onPressed: () {
             
              Navigator.pushNamed(
                //aqui fijarse para el direccionamiento
                context,
                "/mapacomentario",
                
              );
            },
            child: Text('VER MAPA'),
          ),
          
        ],
      ),
    );
  }
}

